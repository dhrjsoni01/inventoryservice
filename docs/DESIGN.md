# HelpingHand

##what is this?

This is a design document for the inventory microservice. All decisions related to schema, API, tech stack will be written here.

## Things to keep in mind

1. following kinds of users would interact with inventory,
  
  (i) admin (or tech support)
  
  (ii) NGO
  
2. a given NGO should be able to register with the inventory service. It should be able to de-register or delete itself too.
  
3. information about donated blood needs to be stored. (id, id of donor, blood group, blood quantity, donation date, final cost for a patient)
  
4. information about NGO needs to be stored took
  
5. NGO can add/remove/update/see all its inventory data.
  
6. transaction data needs to be stored. (transaction id, id of NGO, patient id, amount, quantity of blood, mode of payment, date)
  

## SQL or NoSQL?

1. we need to store transaction data, inventory data (or blood data), and NGO data.
2. the schema for this would remain fixed.
3. displayed data needs to be latest and accurate (consistency)
4. service should serve a response to every request. Dealing with blood and transactions should not be a case where 90% or 95% of requests are served. (availability)
5. from the above, CAP theorem dictates that the system should use a SQL database.

## Schema

[https://dbdiagram.io/d/60cdabe00c1ff875fcd588aa](https://dbdiagram.io/d/60cdabe00c1ff875fcd588aa)

## Tech stack

Node.js, Express, TypeScript, Postgresql (Sequelize for ORM)

## API design

- **GET /bank**
  
  get a list of rows of all blood banks.
  status codes: {200, 400, 404, 500}

- **GET /bank/:bankId**
  
  get bank details for bank
  status codes: {200, 400, 404, 500}

- **POST /bank**
  
  add a new bank, request body data should be in JSON format, w.r.t. schema
  status codes: {201, 400, 500}

- **PUT /bank/:bankId**

  update data
  status codes: {204, 400, 404, 500}

- **DELETE /bank/:bankId**
    
  delete bank
  status codes: {200, 400, 404, 500}

- **GET /bank/:bankId/transaction**
  
  get list of rows of all transactions for a blood bank
  status codes: {200, 400, 404, 500}

- **GET /bank/:bankId/inventory**
  
  get a list of rows of all inventory data that belongs to a given blood bank
  status codes: {200, 400, 404, 500}

- **POST /bank/:bankId/inventory**
  
  add new inventory data for given bank, request body data Jin JSON format, w.r.t. schema
  status codes: {201, 400, 500}

- **PUT /bank/:bankId/inventory/:itemId**
  update inventory item
  status codes: {204, 400, 404, 500}

- **DELETE /bank/:bankId/inventory/:itemId**
  Delete inventory item
  status codes: {204, 400, 404, 500}

- **GET /transaction/:txnId**

  get get detail of single transaction
  status codes: {200, 400, 404, 500}

- **GET /transaction**
  
  get list of rows of all transactions
  status codes: {200, 400, 404, 500}
  
- **POST /transaction**
  
  add new transaction data, request body data in JSON format, w.r.t. shcema
  status codes: {201, 400, 500}

- **POST /find/blood**
  
  find blood banks with given data in body
  eg. location, bloodgroup etc.
  status codes: {201, 400, 500}


    
## File structure

```
root/
├─ node_modules/
├─ docs/
|  ├─ CONTRIBUTING.md 
|  ├─ API.md 
├─ src/
|  ├─ api/ 
│  ├─ services/
│  ├─ models/
│  ├─ config/
│  ├─ app.ts
├─ tests/
│  ├─ services/
├─ package.json
├─ tsconfig.json
├─ .eslintrc.js
├─ .prettierrc.js
├─ .gitignore
├─ .eslintignore
├─ .prettierignore
├─ LICENSE
├─ README.md
```

## Tests and API docs

unit tests would be written for src/services/*

need to find a way to generate MarkDown docs from code, solution: [https://www.npmjs.com/package/apidoc-markdown](https://www.npmjs.com/package/apidoc-markdown)

## Models

schema.ts file: contains schema

Helper.ts file: an abstract class that contains basic methods like establishing connection

index.ts file: use schema.ts and Helper.ts and build out models that can be exported

## Services

Bank.ts: contains Bank class

Transaction.ts: contains Transaction class

Inventory.ts: contains Inventory class

## API

this will use methods from the "services" layer which further would use functions from the "models" layer

general flow: API layer ↔ Service Layer ↔ Data Layer